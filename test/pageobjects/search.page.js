//const Page = require('./page');

class SearchPage {
    //selectors
    get allowCookies() { return $('.optanon-button-allow')}
    get declineCookies() { return $('.optanon-button-close')}
    get h1Text () { return $('h1[data-e2e-heading]') }
    get h2Text () { return $$('h2[data-e2e-heading]')}
    get h3Text () { return $$('h3[data-e2e-heading]') }
    get desktopFilters () { return $$('h3[data-e2e-heading]') }
    get checkboxLabelText () { return $$('span[data-e2e-checkbox-label-text]')}
    get clearFiltersButton () { return $('[data-component=ClearFilterButton]')}
    get button () { return $$('button[data-e2e-button]')}
    get vehicleCard () { return $$('div[data-e2e-card]')}
    get searchInput () { return $('input[data-e2e-text-input-input]')}
    get minimumPriceSlider () { return $('.rc-slider-handle-1')}
    get maximumPriceSlider () { return $('.rc-slider-handle-2')}
    get carPrice () { return $$('[data-component=LocalizedPrice]')}

    //functions
    acceptCookies () {
        if(this.allowCookies.isDisplayed()) {
            this.allowCookies.click();
        }
    }

    buttonName (name) {
        for(let i = 0; i < this.button.length; i++){
            if(this.button[i].getText().toLowerCase() === name){
                this.button[i].click();
            }
        }
    }

    checkVehicleCards (name) {
        for(let i = 0; i < this.vehicleCard.length; i++){
            expect(this.h2Text[i].getText().toLowerCase()).toContain(name);
        }
    }

    selectFilter(filter){
        for(let i = 0; i < this.desktopFilters.length; i++){
            if(this.desktopFilters[i].getText().toLowerCase().includes(filter)){
                this.desktopFilters[i].click();
            }
        }
    }

    selectCheckbox(checkboxLabel){
        for(let i = 0; i < this.checkboxLabelText.length; i++){
            if(this.checkboxLabelText[i].getText().toLowerCase().includes(checkboxLabel)){
                this.checkboxLabelText[i].click();
                this.buttonName("save");
            }
        }
    }

    searchMake (make) {
        this.searchInput.waitForDisplayed();
        this.searchInput.setValue(make);
        browser.keys("\uE007");
    }
}

module.exports = new SearchPage();
