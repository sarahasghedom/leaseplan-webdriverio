const { Given, When, Then } = require('cucumber');
const SearchPage = require('../pageobjects/search.page');

Given(/^I am on the showroom page$/, () => {
    browser.url('https://www.leaseplan.com/en-be/business/showroom/');
    SearchPage.acceptCookies();
});

When(/^I filter on (.*)$/, (filter) => {
    SearchPage.selectFilter(filter);
});

When(/^I select checkbox (.*)$/, (checkboxLabel) => {
    SearchPage.selectCheckbox(checkboxLabel);
});

When(/^I search on make (.*)$/, (make) => {
    SearchPage.searchMake(make);
    expect(SearchPage.checkboxLabelText[0].getText().toLowerCase()).toContain(make);
    SearchPage.selectCheckbox(make);
});

Then(/^I select minimum price (.*)$/, (price) => {
    let minimumPrice = parseInt(price);
    SearchPage.minimumPriceSlider.dragAndDrop({x:minimumPrice, y:0});
});

Then(/^I select maximum price (.*)$/, (price) => {
    let maximumPrice = parseInt(price);
    SearchPage.maximumPriceSlider.dragAndDrop({x:maximumPrice, y:0});
});


Then(/^Cars are filtered with prices between (.*) and (.*)$/, (minimum, maximum) => {
    let minimumPrice = parseInt(minimum);
    let maximumPrice = parseInt(maximum);
    for(let i = 0; i < SearchPage.carPrice.length; i++){
        let leasePrice = parseInt(SearchPage.carPrice[i].getText().replace(/\W+/g, ""));
        expect(leasePrice).toBeGreaterThanOrEqual(minimumPrice);
        expect(leasePrice).toBeLessThanOrEqual(maximumPrice);
    }
});

Then(/^I click on the button (.*)$/, (button) => {
    SearchPage.buttonName('save');
});

Then(/^Cars of the make (.*) are displayed$/, (make) => {
    SearchPage.checkVehicleCards(make);
});

Then(/^The title (.*) is displayed$/, (title) => {
    expect(SearchPage.vehicleCard.length).toBeGreaterThan(0);
    expect(SearchPage.h1Text.getText().toLowerCase()).toEqual(title);
});

When(/^The filter is removed$/, () => {
   SearchPage.clearFiltersButton.click();
});

Then(/The subtitle (.*) is displayed$/, (title) => {
    for(let i = 0; i < SearchPage.h3Text.length; i++){
        if(SearchPage.h3Text[i].getText().toLowerCase() === title){
            expect(SearchPage.h3Text[i].getText().toLowerCase()).toEqual(title);
        }
    }
});
