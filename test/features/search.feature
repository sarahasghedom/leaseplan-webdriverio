Feature: Leaseplan business lease cars

  Background: Open showroom page
    Given I am on the showroom page

  Scenario Outline: Filter works correctly
    When I filter on <filterName>
    And I select checkbox <checkboxFilter>
    Then The title <title> is displayed
    When The filter is removed
    Then The subtitle "best deals" is displayed

    Examples:
      | filterName      | checkboxFilter | title                        |
      | popular filters | best deals     | our business lease cars      |
      | make & model    | audi           | our audi business lease cars |

    Scenario Outline: Search on make & model
      When I filter on make & model
      When I search on make <make>
      Then The title <title> is displayed
      And Cars of the make <make> are displayed
      And The filter is removed

      Examples:
      | make  | title                        |
      | bmw   | our bmw business lease cars  |
      | audi  | our audi business lease cars |
      | seat  | our seat business lease cars |

    # minimum lease price is 200 euro and maximum is 3890 euro
    # If we enter 1 as the minimum price then the slider will increase the price with 15 euro.
    # If we enter 10 the minimum price will increase with 150 euro
    Scenario: Filter on price
      When I filter on monthly price
      Then I select minimum price 10
      And I select maximum price -150
      And I select maximum price 10
      And I click on the button save
      Then Cars are filtered with prices between 200 and 2000
