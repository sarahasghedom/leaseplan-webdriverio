#Leaseplan assignment
### Installing

Clone the project, run one of the following commands

```
git clone git@gitlab.com:sarahasghedom/leaseplan-webdriverio.git #SSH
git clone https://gitlab.com/sarahasghedom/leaseplan-webdriverio.git #HTTPS
```

Open terminal and navigate to the folder to install project

```
npm install
```
## Running the tests

Open terminal and navigate to the folder to run tests

```
npm run test
```

### Generating reports

After running the tests you can generate an Allure report by running the following command

```
npm run report
```
Wait some time until report is generated, a browser will be opened with the HTML report. 
